//1.Описати своїми словами навіщо потрібні функції у програмуванні.
//Функції покликані для оптимізації коду та виконння одинакових дій для великої кількості данних. 

//2.Описати своїми словами, навіщо у функцію передавати аргумент.
//Агрумент - Це значення яке необхідно для отпримання результату функції. Агрументи передаюсться разом із викликом функції.

//3.Що таке оператор return та як він працює всередині функції?
//Оператор return покликан для повернення результату роботи функції. Вісля виклику return функція перестає виконуватись.


//Завдання:
let numberFirst = prompt("Tell me first number", "");
let numberSecond = prompt("Tell me second number", "");
let mathAct = prompt("What mathematical action we will do?", "");

while (numberFirst === null || numberFirst.length === 0) {
    numberFirst = prompt("Tell me first number", numberFirst);
}

while (numberSecond === null || numberSecond.length === 0) {
    numberSecond = prompt("Tell me second number", numberSecond);
}

console.log(mathActResult(+numberFirst,+numberSecond,mathAct));

function mathActResult (numFirst, numSecond, Act) {
    switch (Act) {
        case "+":
            return numFirst+numSecond;
        case "-":
            return numFirst-numSecond;
        case "*":
            return numFirst*numSecond;
        case "/":
            while (numSecond===0) {
                alert("We can`t do it!");
                break
            }
            return numFirst/numSecond;
        default:
            alert("There is no mathematical action to do!");
    }
}